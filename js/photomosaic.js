(function ($) {
  Drupal.behaviors.movemberMosaic = {
    attach: function (context, settings) {
      $("#thumbs .thumb img").mouseenter(function(){
        $(this).addClass("large");
      }).mouseleave(function(){
        $(this).removeClass("large");
      });
    }
  };
})(jQuery);