<?php

/**
 * Form callback from hook_menu() above
 */
function photomosaic_settings_form($form, &$form_state) {
  for ($i = 1; $i < 6; $i++) {
    // If there is already an uploaded image display the image here.
    if ($image_fid = variable_get('photomosaic_image_fid_' . $i, FALSE)) {
      $image = file_load($image_fid);
      $form['image_' . $i] = array(
        '#markup' => theme('image_style', array('style_name' => 'photomosaic_thumbnail', 'path' => $image->uri, 'getsize' => FALSE)),
      );
    }

    // Use the #managed_file FAPI element to upload an image file.
    $form['photomosaic_image_fid_' . $i] = array(
      '#title' => t('Image'),
      '#type' => 'managed_file',
      '#default_value' => variable_get('photomosaic_image_fid_' . $i, ''),
      '#upload_location' => 'public://',
    );
  }

  // Submit Button.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}


/**
 * Verifies that the user supplied an image with the form..
 *
 * @ingroup image_example
 */
function photomosaic_settings_form_validate($form, &$form_state) {
  for ($i = 1; $i < 6; $i++) {
    if (!isset($form_state['values']['photomosaic_image_fid_' . $i]) || !is_numeric($form_state['values']['photomosaic_image_fid_' . $i])) {
      form_set_error('photomosaic_image_fid_' . $i, t('Please select an image to upload.'));
    }
  }
}


/**
 * Form Builder; Display a form for uploading an image.
 *
 * @ingroup image_example
 */
function photomosaic_settings_form_submit($form, &$form_state) {
  for ($i = 1; $i < 6; $i++) {
    // When using the #managed_file form element the file is automatically
    // uploaded an saved to the {file} table. The value of the corresponding
    // form element is set to the {file}.fid of the new file.
    // If fid is not 0 we have a valid file.
    if ($form_state['values']['photomosaic_image_fid_' . $i] != 0) {
      // The new file's status is set to 0 or temporary and in order to ensure
      // that the file is not removed after 6 hours we need to change it's status
      // to 1. Save the ID of the uploaded image for later use.
      $file = file_load($form_state['values']['photomosaic_image_fid_' . $i]);
      $file->status = FILE_STATUS_PERMANENT;
      file_save($file);
      // When a module is managing a file, it must manage the usage count.
      // Here we increment the usage count with file_usage_add().
      file_usage_add($file, 'photomosaic', 'photomosaic_image_fid_' . $i, 1);
      // Save the fid of the file so that the module can reference it later.
      variable_set('photomosaic_image_fid_' . $i, $file->fid);
      drupal_set_message(t('The image @image_name was uploaded and saved with an ID of @fid.', array('@image_name' => $file->filename, '@fid' => $file->fid, '@style' => $form_state['values']['image_example_style_name'])));
    }
    // If the file was removed we need to remove the module's reference to the
    // removed file's fid, and remove the file.
    elseif ($form_state['values']['photomosaic_image_fid_' . $i] == 0) {
      // Retrieve the old file's id.
      $fid = variable_get('photomosaic_image_fid', FALSE);
      $file = $fid ? file_load($fid) : FALSE;
      if ($file) {
        // When a module is managing a file, it must manage the usage count.
        // Here we decrement the usage count with file_usage_delete().
        file_usage_delete($file, 'photomosaic', 'photomosaic_image_fid_' . $i, 1);
        // The file_delete() function takes a file object and checks to see if
        // the file is being used by any other modules. If it is the delete
        // operation is cancelled, otherwise the file is deleted.
        file_delete($file);
      }
      // Either way the module needs to update it's reference since even if the
      // file is in use by another module and not deleted we no longer want to
      // use it.
      variable_set('photomosaic_image_fid_' . $i, FALSE);
      drupal_set_message(t('The image @image_name was removed.', array('@image_name' => $file->filename)));
    }
  }
}
